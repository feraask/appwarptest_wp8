﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using AppWarpTest_WP8.Resources;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using com.shephertz.app42.gaming.multiplayer.client;
using System.Diagnostics;

namespace AppWarpTest_WP8
{
	public partial class MainPage : PhoneApplicationPage
	{
		public ObservableCollection<string> messageList { get; set; }

		// Constructor
		public MainPage()
		{
			InitializeComponent();
			AppServices.InitAppWarp(this);
			this.messageList = new ObservableCollection<string>();
			this.messageList.CollectionChanged += this.messages_CollectionChanged;
			this.DataContext = this;
		}

		private void connectButton_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrWhiteSpace(this.inputBox.Text))
				this.inputBox.Text = "player";
			this.PrintMessage("Connecting with username: " + this.inputBox.Text);
			WarpClient.GetInstance().Connect(this.inputBox.Text);
			this.inputBox.Text = "";
		}

		private void joinRoomButton_Click(object sender, RoutedEventArgs e)
		{
			// Create a room if no roomID is provided
			if (string.IsNullOrWhiteSpace(this.inputBox.Text))
			{
				this.PrintMessage("creating a new room...");
				WarpClient.GetInstance().CreateRoom("room", "user", 5, null);
			}
			else
			{
				this.PrintMessage("joining & subscribing to room: " + this.inputBox.Text);
				WarpClient.GetInstance().JoinRoom(this.inputBox.Text);
				WarpClient.GetInstance().SubscribeRoom(this.inputBox.Text);
			}
			this.inputBox.Text = "";
		}

		private void disconnectButton_Click(object sender, RoutedEventArgs e)
		{
			this.PrintMessage("Disconnecting...");
			WarpClient.GetInstance().Disconnect();
		}

		private void clearButton_Click(object sender, RoutedEventArgs e)
		{
			this.ClearMessages();
		}

		public void PrintMessage(string message)
		{
			this.Dispatcher.BeginInvoke(() =>
			{
				this.messageList.Add(message);
				Debug.WriteLine(message);
			});
		}

		public void ClearMessages()
		{
			this.Dispatcher.BeginInvoke(() =>
			{
				this.messageList.Clear();
			});
		}

		private void messages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			var selectedIndex = this.messageListView.Items.Count - 1;
			if (selectedIndex < 0)
				return;

			this.messageListView.SelectedIndex = selectedIndex;
			this.messageListView.UpdateLayout();

			this.messageListView.ScrollIntoView(this.messageListView.SelectedItem);
		}
	}
}