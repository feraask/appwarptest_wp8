﻿using com.shephertz.app42.gaming.multiplayer.client;
using com.shephertz.app42.gaming.multiplayer.client.command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppWarpTest_WP8
{
	public class AppServices
	{
		public static string apiKey = "REPLACE_WITH_API_KEY";
		public static string secretKey = "REPLACE_WITH_SECRET_KEY";

		public static void InitAppWarp(MainPage page)
		{
			WarpClient.initialize(AppServices.apiKey, AppServices.secretKey);
			WarpClient.setRecoveryAllowance(30);
			WarpClient.GetInstance().AddConnectionRequestListener(new ConnectionListener(page));
			WarpClient.GetInstance().AddNotificationListener(new NotificationListener(page));
			WarpClient.GetInstance().AddUpdateRequestListener(new UpdateListener(page));
			WarpClient.GetInstance().AddRoomRequestListener(new RoomListener(page));
			WarpClient.GetInstance().AddZoneRequestListener(new ZoneListener(page));
		}
	}


	public class ConnectionListener : com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener
	{
		public MainPage _page = null;

		public ConnectionListener()
		{ }

		public ConnectionListener(MainPage page)
		{ this._page = page; }

		public async void onConnectDone(com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent eventObj)
		{
			byte result = eventObj.getResult();
			string msg = null;

			switch (result)
			{
				case WarpResponseResultCode.SUCCESS:
					msg = "Connected Successfully!";
					break;
				case WarpResponseResultCode.AUTH_ERROR:
					_page.PrintMessage("Connect AUTH_ERROR");
					//byte reasonCode = eventObj.getReasonCode();
					//switch (reasonCode)
					//{
					//	case WarpReasonCode.WAITING_FOR_PAUSED_USER:
					//		msg = "Connect AUTH_ERROR Reason Code: Waiting for Paused User (" + reasonCode + ")";
					//		break;
					//	default:
					//		msg = "Connect AUTH_ERROR Reason Code: " + reasonCode;
					//		break;
					//}
					break;
				case WarpResponseResultCode.SUCCESS_RECOVERED:
					msg = "Connection Recovered!";
					break;
				case WarpResponseResultCode.CONNECTION_ERROR_RECOVERABLE:
					_page.PrintMessage("Connection error recoverable occured. waiting 15 seconds to recover...");
					await Task.Delay(15000);
					_page.PrintMessage("recovering...");
					WarpClient.GetInstance().RecoverConnection();
					return;
				default:
					msg = "unknown connection error occured";
					break;
			}

			_page.PrintMessage(msg);
		}

		public async void onDisconnectDone(com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent eventObj)
		{
			byte result = eventObj.getResult();
			string msg = null;

			switch (result)
			{
				case WarpResponseResultCode.SUCCESS:
					msg = "Disconnect Successfully!";
					break;
				case WarpResponseResultCode.AUTH_ERROR:
					_page.PrintMessage("DISCONNECT AUTH_ERROR");
					//byte reasonCode = eventObj.getReasonCode();
					//switch (reasonCode)
					//{
					//	case WarpReasonCode.WAITING_FOR_PAUSED_USER:
					//		msg = "Disconnect AUTH_ERROR Reason Code: Waiting for Paused User (" + reasonCode + ")";
					//		break;
					//	default:
					//		msg = "Disconnect AUTH_ERROR Reason Code: " + reasonCode;
					//		break;
					//}
					break;
				default:
					msg = "unknown disconnect error occured";
					break;
			}

			_page.PrintMessage(msg);
		}


		public void onInitUDPDone(byte resultCode)
		{

		}
	}

	public class NotificationListener : com.shephertz.app42.gaming.multiplayer.client.listener.NotifyListener
	{
		private MainPage _page;
		public NotificationListener()
		{ }

		public NotificationListener(MainPage currPage)
		{ _page = currPage; }

		public void onChatReceived(com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent eventObj)
		{
		}

		public void onGameStarted(string sender, string roomId, string nextTurn)
		{
		}

		public void onGameStopped(string sender, string roomId)
		{
		}

		public void onMoveCompleted(com.shephertz.app42.gaming.multiplayer.client.events.MoveEvent moveEvent)
		{
		}

		public void onPrivateChatReceived(string sender, string message)
		{
		}

		public void onPrivateUpdateReceived(string sender, byte[] update, bool fromUdp)
		{
		}

		public void onRoomCreated(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj)
		{
		}

		public void onRoomDestroyed(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj)
		{
		}

		public void onUpdatePeersReceived(com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent eventObj)
		{
		}

		public void onUserChangeRoomProperty(com.shephertz.app42.gaming.multiplayer.client.events.RoomData roomData, string sender, Dictionary<string, object> properties, Dictionary<string, string> lockedPropertiesTable)
		{
		}

		public void onUserJoinedLobby(com.shephertz.app42.gaming.multiplayer.client.events.LobbyData eventObj, string username)
		{
		}

		public void onUserLeftLobby(com.shephertz.app42.gaming.multiplayer.client.events.LobbyData eventObj, string username)
		{
		}

		public async void onUserJoinedRoom(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj, string username)
		{
			_page.PrintMessage(username + " joined room");
		}

		public async void onUserLeftRoom(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj, string username)
		{
			_page.PrintMessage(username + " left room");
		}

		public async void onUserPaused(string locid, bool isLobby, string username)
		{
			_page.PrintMessage(username + " paused");
		}

		public async void onUserResumed(string locid, bool isLobby, string username)
		{
			_page.PrintMessage(username + " resumed with connection state: " + WarpClient.GetInstance().GetConnectionState());
		}

		public void onNextTurnRequest(string lastTurn)
		{
		}
	}

	public class UpdateListener : com.shephertz.app42.gaming.multiplayer.client.listener.UpdateRequestListener
	{
		public MainPage _page;

		public UpdateListener()
		{
		}

		public UpdateListener(MainPage currPage)
		{
			_page = currPage;
		}

		public void onSendPrivateUpdateDone(byte result)
		{

		}

		public void onSendUpdateDone(byte result)
		{

		}
	}

	public class RoomListener : com.shephertz.app42.gaming.multiplayer.client.listener.RoomRequestListener
	{
		private MainPage _page;
		public RoomListener(MainPage page)
		{ this._page = page; }

		public void onGetLiveRoomInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
		{
		}

		public async void onJoinRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
			{
				string roomID = eventObj.getData().getId();
				_page.PrintMessage("joined room: " + roomID);
			}
			else
			{
				_page.PrintMessage("join room error code: " + eventObj.getResult());
			}
		}

		public void onLeaveRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onLockPropertiesDone(byte result)
		{
		}

		public void onSetCustomRoomDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
		{
		}

		public async void onSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
			{
				string roomID = eventObj.getData().getId();
				_page.PrintMessage("subscribed to room: " + roomID);
			}
			else
			{
				_page.PrintMessage("subscribe room error code: " + eventObj.getResult());
			}
		}

		public void onUnSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onUnlockPropertiesDone(byte result)
		{
		}

		public void onUpdatePropertyDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent lifeLiveRoomInfoEvent)
		{
		}
	}

	public class ZoneListener : com.shephertz.app42.gaming.multiplayer.client.listener.ZoneRequestListener
	{
		private MainPage _page;

		public ZoneListener(MainPage page)
		{ this._page = page; }

		public async void onCreateRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			string roomID = eventObj.getData().getId();
			_page.PrintMessage("created room id: " + roomID);
			_page.PrintMessage("joining & subscribing to room...");
			WarpClient.GetInstance().JoinRoom(roomID);
			WarpClient.GetInstance().SubscribeRoom(roomID);
		}

		public void onDeleteRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onGetAllRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent eventObj)
		{
		}

		public void onGetLiveUserInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
		{
		}

		public void onGetMatchedRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent matchedRoomsEvent)
		{
		}

		public void onGetOnlineUsersDone(com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent eventObj)
		{
		}

		public void onSetCustomUserDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
		{
		}
	}

}
